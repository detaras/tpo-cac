const limpiarFormulario = () => {
    // document.getElementById("formulario").reset(); 
    document.querySelector("#formulario").reset();
    console.log("Formulario limpio...");
};

const enviarFormulario = () => {
    document.querySelector("#errorNombre").style.display = 'none';
    document.querySelector("#errorApellido").style.display = 'none';
    document.querySelector("#errorEmail").style.display = 'none';
    document.querySelector("#errorConsulta").style.display = 'none';

    let nombre = document.querySelector("#nombre").value;
    let apellido = document.querySelector("#apellido").value;
    let email = document.querySelector("#email").value;
    let telefono = document.querySelector("#telefono").value;
    let consulta = document.querySelector("#consulta").value;

    if( nombre == "" ) {
        document.querySelector("#errorNombre").style.display = 'block';
        return;
    } else if( apellido == "" ) {
        document.querySelector("#errorApellido").style.display = 'block';
        return;
    } else if( email == "" ) {
        document.querySelector("#errorEmail").style.display = 'block';
        return;
    } else if( consulta == "" ) {
        document.querySelector("#errorConsulta").style.display = 'block';
        return;
    }

    document.querySelector("#errorNombre").style.display = 'none';
    document.querySelector("#errorApellido").style.display = 'none';
    document.querySelector("#errorEmail").style.display = 'none';
    document.querySelector("#errorConsulta").style.display = 'none';
    document.querySelector("#consultaEnviada").style.display = 'block';
    document.querySelector("#formulario").reset();
}
